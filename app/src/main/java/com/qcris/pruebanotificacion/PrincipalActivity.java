package com.qcris.pruebanotificacion;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

/**
 * Basado en https://www.androidauthority.com/how-to-create-android-notifications-707254/
 */

public class PrincipalActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_principal);
	}

	public void sendNotification(View view) {

		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);

		//Create the intent that’ll fire when the user taps the notification//

		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.androidauthority.com/"));
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

		mBuilder.setContentIntent(pendingIntent);

		mBuilder.setSmallIcon(R.mipmap.ic_launcher);
		mBuilder.setContentTitle("My notification");
		mBuilder.setContentText("Hello World!");

		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		mNotificationManager.notify(001, mBuilder.build());
	}
}
